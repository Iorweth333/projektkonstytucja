package agh.cs.konstytucja;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;

public class Reader {

	
	//LinkedList<Car> carList = new LinkedList<Car>();
	
	public String nazwaPliku;
	public FileReader plik;
	public BufferedReader bufor;
	
	public Reader (String nazwaPliku) throws FileNotFoundException {		//konstruktor
		this.nazwaPliku = nazwaPliku;
		this.plik = openFile (nazwaPliku);
		this.bufor = new BufferedReader (this.plik);
	}

	
	public FileReader openFile(String fileName) throws FileNotFoundException {		
		FileReader plik = new FileReader(fileName);
		return plik;
	}


	public LinkedList <String> wczytajTekst (String konczacaLinijka) throws IOException {
		String biezacaLinia = null;
		LinkedList <String> listaLinii = new LinkedList <String>();
		
		boolean koniec = false;
		
		while (!koniec){
			biezacaLinia=this.bufor.readLine();
			
			if (!biezacaLinia.equals(konczacaLinijka)) listaLinii.add (biezacaLinia);
			else koniec = true;		
		}
		if (biezacaLinia.equals("dnia jej ogłoszenia.")) listaLinii.add (biezacaLinia);	//bo mi ucina ostatnią linię, muszę dodać sztucznie
		
		return listaLinii;
		
	}
	

}
