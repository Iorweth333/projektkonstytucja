package agh.cs.konstytucja;

import static java.lang.System.out;

public class WrongEntryException extends Exception {
	
	private static final long serialVersionUID = 1L;		//jakieś ID, eclipse krzyczał, że chce, dałem default

	public WrongEntryException (){
		System.out.print(" Niepoprawna instrukcja wejściowa.");
		out.println(" Popraw instrukcje wejściowe i uruchom program jeszcze raz.");
		out.println(
				" Przykład poprawnej instrukcji wejścia: \"Rozdział 1\", \"Artykuł 2\", \"Artykuły 3 6\" [jest to przedział artykułów od 3 do 6]");
		out.println(
				" Ważne: rozdziałów jest 13 (XIII), artykułów 243.\n Należy zaczynać wyrazy z wielkiej litery, używać liczb arabskich dla artykułów i rzymskich dla rozdziałów, robić odstępy");
		out.println(
				" Polskie znaki niewymagane. W przypadku wyświetlania pojedynczych artykułów, działają formy skrócone: \"Art\" i \"Art.\"");
		out.println(" Po instrukcji powinna znajdować się ŚCIEŻKA DO PLIKU.");
	}

}
