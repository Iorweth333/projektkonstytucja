package agh.cs.konstytucja;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.LinkedList;

import static java.lang.System.out;

public class Konstytucja {

	public static void main(String[] args) {


		String[] instrukcje = null;
		try {
			instrukcje = new Parsers().entryInterpreter(args);
		} catch (WrongEntryException ex) {
			System.exit(0);
		}
		
		
		String sciezkaPliku = instrukcje[instrukcje.length-1];


		Reader plik = null;
		try {
			plik = new Reader(sciezkaPliku);

		} catch (FileNotFoundException ex) {
			out.println(ex.getMessage());
			out.println("Podaj poprawną ścieżkę do pliku i uruchom program jeszcze raz");
			System.exit(0);
		}
		

		
		try {
			plik = new Finder().findText (plik, instrukcje [0]);	//szukam, gdzie zacząć wypisywanie
		} catch (IOException e) {
			out.println (e.getMessage());
			e.printStackTrace();
			System.exit(0);
		}
		
		LinkedList <String> listaLinii = null;		//tu będą przechowane linijki tekstu do przeparsowania i wyświetlenia 
		
		try {
			listaLinii = plik.wczytajTekst(instrukcje[1]);
		} catch (IOException e) {
			e.printStackTrace();
			
			System.exit(0);
		}
		
		listaLinii = new Parsers().parseText(listaLinii);
		
		out.println(instrukcje[0]);
		Printer drukarz = new Printer (listaLinii);
		drukarz.printText();
		
		
	}

}
