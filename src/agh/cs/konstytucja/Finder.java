package agh.cs.konstytucja;

import java.io.IOException;

public class Finder{

	
	public Reader findText(Reader plik, String textToFind) throws IOException {
		String trescLinii;
		while (plik.bufor != null){
			trescLinii = plik.bufor.readLine();
			if (trescLinii.equals(textToFind)) break;
		}
		
		return plik;
	}
}
