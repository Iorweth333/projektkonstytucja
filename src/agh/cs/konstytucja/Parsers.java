package agh.cs.konstytucja;

import java.util.Iterator;
import java.util.LinkedList;

public class Parsers {

	public LinkedList <String> parseText(LinkedList <String> listaLinii) {
		
		Iterator <String> iterator = listaLinii.iterator(); 
		
		while (iterator != null && iterator.hasNext()){
			String temp = iterator.next();
			
			if (temp.equals("©Kancelaria Sejmu") || temp.equals("2009-11-16"))		//tu jest taka trochę dziwnostka, bo remove usuwa to, co ostatnio zwrócił iterator (czy jakoś tak) w każdym razie sam bym na to nie wpadł
				iterator.remove();
			
		}
		
		//w sumie by mozna przepisać do tablicy, żeby był szybszy dostęp
		
		//DEBUG
		//System.out.println("Wypisywanie w parserze: ");
		
		
		for (int i=0; i<listaLinii.size(); i++){
			
			String temp2 = listaLinii.get(i);			
			
			if (temp2.endsWith("-") ) {
								
				String temp3 = listaLinii.get(i+1);
				
				//DEBUG
				//System.out.println(temp2);
				//System.out.println(temp3);
				
				
				int spaceIndex = temp3.indexOf(' ');
				String doPrzeniesienia;
				if (spaceIndex != -1){
					doPrzeniesienia = temp3.substring(0, spaceIndex);
					temp3 = temp3.substring(spaceIndex +1);
				}
					
				else{
					doPrzeniesienia = temp3;
					temp3="";
				}
				
				int pauseIndex = temp2.length()-1;
				temp2 = temp2.substring(0, pauseIndex);	//usunięcie pauzy 
				
				temp2 = temp2.concat(doPrzeniesienia);
					
				
				//DEBUG
				//System.out.println("DO PRZENIESIENIA: " + doPrzeniesienia);
				//System.out.println(temp2);
				//System.out.println(temp3);
				
				listaLinii.set(i, temp2); //zastępuje i-ty element 
				listaLinii.set(i + 1, temp3);
				
				if (spaceIndex == -1) listaLinii.remove(i+1);
				
			}
		}
		
		return listaLinii;

	}

	public String[] entryInterpreter(String[] entry) throws WrongEntryException {
		
		if (entry.length < 3) {
			System.out.println ("Za mało argumentów. ");
			throw new WrongEntryException();
		}
		
		if (entry.length >4) {
			System.out.println ("Za dużo argumentów. ");
			throw new WrongEntryException();
		}
		String[] wynik = new String[entry.length];		//na wyjściu ma być: w zerowej pozycji linijka od której zaczynamy, w pierwszej pozycji linijka na kórej kończymy
														//np. [0] <- Rozdział I [1] <- Rozdział II
														//w przypadku ostatniego rozdziału: Rozdział XIII a potem "dnia jej ogłoszenia.", czyli ostatnia linijka
		wynik[wynik.length-1] = entry[entry.length-1];	//na ostatniej pozycji ma być ścieżka pliku
														//UWAGA! PROGRAM DZIAŁA NIE DO KOŃCA POPRAWNIE GDY WYPISZEMY ARTYKUŁ, KTÓRY JEST OSTATNI W DANYM ROZDZIALE!
		
		for (int i = 0; i < entry.length-1; i++) {
			switch (entry [i]){
			
			case "Rozdział":
				wynik [i] = "Rozdział";	//czyli nic się nie zmienia
				break;
			case "Rozdzial":
				wynik[i]="Rozdział";
				break;
			case "Artykuł":
				wynik [i] = "Art.";
				break;
			case "Artykul":
				wynik [i] = "Art.";
				break;
			case "Art.":
				wynik [i] = "Art.";
				break;
			case "Art":
				wynik [i] = "Art.";
				break;
			case "Artykuły":
				wynik [i] = "Artykuły";
				break;
			case "Artykuly":
				wynik [i] = "Artykuły";
				break;
			default: 
				if (isNumber (entry[i]) || isRomanNumber (entry [i])) wynik [i] = entry[i];
				else {
					System.out.print(entry[i] + ":");
					throw new WrongEntryException();
				}
			}
		}
		//na nieszczęście użytkownika, łatwiej wytykać mu błędy niż go poprawiać ;)
		
		if (wynik[0].equals(new String ("Rozdział")) && wynik.length > 3){
			System.out.println("Nie dopuszcza się podawania zakresu rozdziałów!");
			throw new WrongEntryException();
		}
		
		if (wynik[0].equals(new String ("Rozdział")) && !isRomanNumber (wynik[1])) {
			System.out.println("Wraz z rodziałami należy używać liczb rzymskich z przedziału I-XIII!");
			throw new WrongEntryException();
		}
		
		
		if (wynik[0].equals(new String ("Art.")) && wynik.length > 3) {
			System.out.println("Żeby wyświetlić zakres aktykułów, użyj słowa \"Artykuły\"");
			throw new WrongEntryException();
		}
		
		if (wynik[0].equals(new String ("Artykuły")) && wynik.length < 4) {
			System.out.println("Żeby wyświetlić jeden artykuł, użyj słowa \"Artykuł\"");
			throw new WrongEntryException();
		}
		
		if (wynik[0].equals(new String ("Art.")) && isRomanNumber(wynik[1])){
			System.out.println("Wraz artykułami należy używać liczb arabskich!");
			throw new WrongEntryException();
		}
		
		//ogarnianie początkowej i kończącej linii:
		
		if (wynik[0].equals("Rozdział")){				//przykłady:
			wynik[0] = wynik[0] + " " + wynik[1];		//Rozdział I
			if (!wynik [1].equals("XIII")) wynik[1] = "Rozdział " + incRomanNumber (wynik[1]);		//Rozdział II
			else wynik[1] = "dnia jej ogłoszenia.";
		}												
					
			

		if (wynik[0].equals("Art.")){
			wynik [0] = "Art. " + wynik [1] + ".";		//Art. 3.
			if (Integer.parseInt(wynik[1])>243) {
				System.out.println("Artykułów w naszej Konstytucji jest tylko 243!");
				throw new WrongEntryException();
			}
			wynik [1] = "Art. " + Integer.toString(Integer.parseInt(wynik[1]) + 1) + ".";	//ale potworek ;) zamienia string na liczbę, powiększa ją o 1 a następnie z powrotem zamienia ją na string
			
		}
		
		if (wynik[0].equals("Artykuły")) {						
			wynik[0] = "Art. " + wynik[1] + ".";		//Art. 3.
			wynik[1] = "Art. " + Integer.toString(Integer.parseInt(wynik[2]) + 1) + ".";		//Art. 6.
			if (wynik[1].equals("Art. 244.")) wynik[1] = "dnia jej ogłoszenia.";
		}
		
		
		return wynik;
	}
	
	public boolean isNumber (String napis){
		for (int i=0; i<napis.length(); i++){
			if (Character.getNumericValue(napis.charAt(i)) < 0 || Character.getNumericValue(napis.charAt(i)) >9) return false;		//znalezione na http://stackoverflow.com/questions/4968323/java-parse-int-value-from-a-char
		}
		return true;
	}
	public boolean isRomanNumber (String napis){
		switch (napis){
		case "I":
			return true;
		case "II":
			return true;
		case "III":
			return true;
		case "IV":
			return true;
		case "V":
			return true;
		case "VI":
			return true;
		case "VII":
			return true;
		case "VIII":
			return true;
		case "IX":
			return true;
		case "X":
			return true;
		case "XI":
			return true;
		case "XII":
			return true;
		case "XIII":
			return true;
		default : return false;
	
		}
	}
	public String incRomanNumber (String number){
		switch (number){
		case "I":
			return "II";
		case "II":
			return "III";
		case "III":
			return "IV";
		case "IV":
			return "V";
		case "V":
			return "VI";
		case "VI":
			return "VII";
		case "VII":
			return "VIII";
		case "VIII":
			return "IX";
		case "IX":
			return "X";
		case "X":
			return "XI";
		case "XI":
			return "XII";
		case "XII":
			return "XIII";
		default: return "";
		}
	}

}
